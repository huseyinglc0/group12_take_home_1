﻿
using System;

namespace WindowsFormsApp3
{
    partial class SignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.userNameLbl = new System.Windows.Forms.Label();
            this.passwordLbl = new System.Windows.Forms.Label();
            this.rememberCheckBox = new System.Windows.Forms.CheckBox();
            this.signInBtn = new System.Windows.Forms.Button();
            this.userNameTxt = new System.Windows.Forms.TextBox();
            this.passwordTxt = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.leftFrameLbl = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.showMessageLbl = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_SignUp = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // userNameLbl
            // 
            this.userNameLbl.AutoSize = true;
            this.userNameLbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.userNameLbl.Location = new System.Drawing.Point(89, 237);
            this.userNameLbl.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.userNameLbl.Name = "userNameLbl";
            this.userNameLbl.Size = new System.Drawing.Size(115, 27);
            this.userNameLbl.TabIndex = 0;
            this.userNameLbl.Text = "User Name";
            this.userNameLbl.Click += new System.EventHandler(this.label1_Click);
            // 
            // passwordLbl
            // 
            this.passwordLbl.AutoSize = true;
            this.passwordLbl.Location = new System.Drawing.Point(89, 320);
            this.passwordLbl.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.passwordLbl.Name = "passwordLbl";
            this.passwordLbl.Size = new System.Drawing.Size(94, 27);
            this.passwordLbl.TabIndex = 1;
            this.passwordLbl.Text = "Password";
            this.passwordLbl.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // rememberCheckBox
            // 
            this.rememberCheckBox.AutoSize = true;
            this.rememberCheckBox.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rememberCheckBox.Location = new System.Drawing.Point(94, 527);
            this.rememberCheckBox.Name = "rememberCheckBox";
            this.rememberCheckBox.Size = new System.Drawing.Size(138, 27);
            this.rememberCheckBox.TabIndex = 2;
            this.rememberCheckBox.Text = "Remember Me";
            this.rememberCheckBox.UseVisualStyleBackColor = true;
            this.rememberCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // signInBtn
            // 
            this.signInBtn.BackColor = System.Drawing.SystemColors.ControlLight;
            this.signInBtn.Location = new System.Drawing.Point(94, 409);
            this.signInBtn.Name = "signInBtn";
            this.signInBtn.Size = new System.Drawing.Size(290, 47);
            this.signInBtn.TabIndex = 3;
            this.signInBtn.Text = "LOGIN";
            this.signInBtn.UseVisualStyleBackColor = false;
            this.signInBtn.Click += new System.EventHandler(this.signInBtn_Click);
            // 
            // userNameTxt
            // 
            this.userNameTxt.Location = new System.Drawing.Point(94, 267);
            this.userNameTxt.Name = "userNameTxt";
            this.userNameTxt.Size = new System.Drawing.Size(408, 34);
            this.userNameTxt.TabIndex = 4;
            this.userNameTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // passwordTxt
            // 
            this.passwordTxt.Location = new System.Drawing.Point(94, 350);
            this.passwordTxt.Name = "passwordTxt";
            this.passwordTxt.Size = new System.Drawing.Size(408, 34);
            this.passwordTxt.TabIndex = 5;
            this.passwordTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.passwordTxt.UseSystemPasswordChar = true;
            this.passwordTxt.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GrayText;
            this.panel1.Controls.Add(this.leftFrameLbl);
            this.panel1.Location = new System.Drawing.Point(41, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(688, 88);
            this.panel1.TabIndex = 6;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // leftFrameLbl
            // 
            this.leftFrameLbl.AutoSize = true;
            this.leftFrameLbl.BackColor = System.Drawing.SystemColors.GrayText;
            this.leftFrameLbl.Font = new System.Drawing.Font("Constantia", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.leftFrameLbl.ForeColor = System.Drawing.Color.Ivory;
            this.leftFrameLbl.Location = new System.Drawing.Point(174, 22);
            this.leftFrameLbl.Name = "leftFrameLbl";
            this.leftFrameLbl.Size = new System.Drawing.Size(331, 45);
            this.leftFrameLbl.TabIndex = 0;
            this.leftFrameLbl.Text = "Login Application";
            this.leftFrameLbl.Click += new System.EventHandler(this.leftFrameLbl_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(94, 462);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(84, 47);
            this.exitBtn.TabIndex = 7;
            this.exitBtn.Text = "EXIT";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // showMessageLbl
            // 
            this.showMessageLbl.AutoSize = true;
            this.showMessageLbl.Location = new System.Drawing.Point(89, 176);
            this.showMessageLbl.Name = "showMessageLbl";
            this.showMessageLbl.Size = new System.Drawing.Size(106, 27);
            this.showMessageLbl.TabIndex = 8;
            this.showMessageLbl.Text = "Waiting...";
            this.showMessageLbl.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_SignUp
            // 
            this.btn_SignUp.Location = new System.Drawing.Point(390, 409);
            this.btn_SignUp.Name = "btn_SignUp";
            this.btn_SignUp.Size = new System.Drawing.Size(112, 47);
            this.btn_SignUp.TabIndex = 9;
            this.btn_SignUp.Text = "Sign Up";
            this.btn_SignUp.UseVisualStyleBackColor = true;
            this.btn_SignUp.Click += new System.EventHandler(this.btn_SignUp_Click);
            // 
            // SignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(802, 596);
            this.Controls.Add(this.btn_SignUp);
            this.Controls.Add(this.showMessageLbl);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.passwordTxt);
            this.Controls.Add(this.userNameTxt);
            this.Controls.Add(this.signInBtn);
            this.Controls.Add(this.rememberCheckBox);
            this.Controls.Add(this.passwordLbl);
            this.Controls.Add(this.userNameLbl);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "SignIn";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.Label userNameLbl;
        private System.Windows.Forms.Label passwordLbl;
        private System.Windows.Forms.CheckBox rememberCheckBox;
        private System.Windows.Forms.Button signInBtn;
        private System.Windows.Forms.TextBox userNameTxt;
        private System.Windows.Forms.TextBox passwordTxt;
        private System.Windows.Forms.Label leftFrameLbl;
        private System.Windows.Forms.Button exitBtn;
        protected internal System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label showMessageLbl;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn_SignUp;
    }
}

