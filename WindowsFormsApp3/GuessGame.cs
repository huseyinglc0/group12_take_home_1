﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace WindowsFormsApp3
{
    class GuessGame
    {

        private int minX = 0;
        private int highX = 1025;
        private int minY = 0;
        private int highY = 1025;

        private int midPointX = 512;
        private int midPointY = 512;


        public void reset()
        {
            minX = 0;
            highX = 1025;
            minY = 0;
            highY = 1025;
            midPointX = 512;
            midPointY = 512;
        }

        public override string ToString()
        {
            return midPointX + " , " + midPointY;
        }

        public void dogu()
        {
            minX = midPointX;
            midPointX = (minX + highX) / 2;
        }
        public void bati()
        {
            highX = midPointX;
            midPointX = (minX + highX) / 2;
        }

        public void kuzey()
        {
            minY = midPointY;
            midPointY = (highY + minY) / 2;
        }
        public void guney()
        {
            highY = midPointY;
            midPointY = (minY + highY) / 2;
        }

        public void guneydogu()
        {
            guney();
            dogu();
        }

        public void guneybati()
        {
            guney();
            bati();
        }

        public void kuzeydogu()
        {
            kuzey();
            dogu();
        }

        public void kuzeybati()
        {
            kuzey();
            bati();
        }


    }
}
