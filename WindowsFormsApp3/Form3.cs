﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{    
    public partial class Form3 : Form
    {
        

        private static List<User> listUser = new List<User>();
        User newUser = User.Instance();
        public Form3()
        {
            InitializeComponent();
        }
        private void Form3_Load(object sender, EventArgs e)
        {
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void userName_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void userName_Leave(object sender, EventArgs e)
        {
            
        }

        private void label4_Click(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        internal static List<User> ListUser { get => listUser; set => listUser = value; }
        private void signUpBtn_Click(object sender, EventArgs e)
        {

            string path = @"user.csv";
            if (userNameTxt.Text == "" || passwordTxt.Text == "" || confirmpasswordTxt.Text == "" )
            {
                MessageBox.Show("Fill in the Missing Information!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            else if(passwordTxt.Text!= confirmpasswordTxt.Text)
            {
                MessageBox.Show("Password must be match!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            else
            {
                newUser.Username = userNameTxt.Text;
                string passwithHash = User.MD5Hash(passwordTxt.Text);
                newUser.Password = passwithHash;
                string confirmpassHash = User.MD5Hash(confirmpasswordTxt.Text);
                newUser.Confirmpassword = confirmpassHash;

                ListUser.Add(newUser);

                DialogResult ask = new DialogResult();
                ask = MessageBox.Show("Do you want to save ?", "Warning", MessageBoxButtons.YesNo);
                if (ask == DialogResult.Yes)
                {
                    try
                    {
                        File.SaveCsv(ListUser, path);
                        MessageBox.Show("File saved");
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error", ex);
                    }
                }

                if (ask == DialogResult.No)
                {
                    MessageBox.Show("Not saved");
                }

            }           

        }

        public void clearInput()
        {
            userNameTxt.Text = "";
            passwordTxt.Text = "";
            confirmpasswordTxt.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SignIn signIn = new SignIn();
            signIn.Show();
            this.Visible = false;
        }
    }   
}
