﻿
namespace WindowsFormsApp3
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.btncarp = new System.Windows.Forms.Button();
            this.btnsonuc = new System.Windows.Forms.Button();
            this.btnbol = new System.Windows.Forms.Button();
            this.btntopla = new System.Windows.Forms.Button();
            this.btncikar = new System.Windows.Forms.Button();
            this.num1 = new System.Windows.Forms.TextBox();
            this.num2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(12, 26);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(42, 13);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Sayı 1 :\r\n";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(12, 57);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(39, 13);
            this.lbl2.TabIndex = 1;
            this.lbl2.Text = "Sayı 2:";
            // 
            // btncarp
            // 
            this.btncarp.BackColor = System.Drawing.Color.White;
            this.btncarp.Location = new System.Drawing.Point(186, 84);
            this.btncarp.Name = "btncarp";
            this.btncarp.Size = new System.Drawing.Size(75, 23);
            this.btncarp.TabIndex = 2;
            this.btncarp.Text = "ÇARPMA";
            this.btncarp.UseVisualStyleBackColor = false;
            this.btncarp.Click += new System.EventHandler(this.btncarp_Click);
            this.btncarp.MouseLeave += new System.EventHandler(this.btncarp_MouseLeave);
            this.btncarp.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btncarp_MouseMove);
            // 
            // btnsonuc
            // 
            this.btnsonuc.BackColor = System.Drawing.Color.White;
            this.btnsonuc.Location = new System.Drawing.Point(105, 113);
            this.btnsonuc.Name = "btnsonuc";
            this.btnsonuc.Size = new System.Drawing.Size(75, 23);
            this.btnsonuc.TabIndex = 3;
            this.btnsonuc.Text = "SONUC";
            this.btnsonuc.UseVisualStyleBackColor = false;
            this.btnsonuc.Click += new System.EventHandler(this.btnsonuc_Click);
            this.btnsonuc.MouseLeave += new System.EventHandler(this.btnsonuc_MouseLeave);
            this.btnsonuc.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnsonuc_MouseMove);
            // 
            // btnbol
            // 
            this.btnbol.BackColor = System.Drawing.Color.White;
            this.btnbol.Location = new System.Drawing.Point(186, 113);
            this.btnbol.Name = "btnbol";
            this.btnbol.Size = new System.Drawing.Size(75, 23);
            this.btnbol.TabIndex = 4;
            this.btnbol.Text = "BÖLME";
            this.btnbol.UseVisualStyleBackColor = false;
            this.btnbol.Click += new System.EventHandler(this.btnbol_Click);
            this.btnbol.MouseLeave += new System.EventHandler(this.btnbol_MouseLeave);
            this.btnbol.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnbol_MouseMove);
            // 
            // btntopla
            // 
            this.btntopla.BackColor = System.Drawing.Color.White;
            this.btntopla.Location = new System.Drawing.Point(186, 23);
            this.btntopla.Name = "btntopla";
            this.btntopla.Size = new System.Drawing.Size(75, 23);
            this.btntopla.TabIndex = 5;
            this.btntopla.Text = "TOPLAMA";
            this.btntopla.UseVisualStyleBackColor = false;
            this.btntopla.Click += new System.EventHandler(this.btntopla_Click);
            this.btntopla.MouseLeave += new System.EventHandler(this.btntopla_MouseLeave);
            this.btntopla.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btntopla_MouseMove);
            // 
            // btncikar
            // 
            this.btncikar.BackColor = System.Drawing.Color.White;
            this.btncikar.Location = new System.Drawing.Point(186, 54);
            this.btncikar.Name = "btncikar";
            this.btncikar.Size = new System.Drawing.Size(75, 23);
            this.btncikar.TabIndex = 6;
            this.btncikar.Text = "ÇIKARMA";
            this.btncikar.UseVisualStyleBackColor = false;
            this.btncikar.Click += new System.EventHandler(this.btncikar_Click);
            this.btncikar.MouseLeave += new System.EventHandler(this.btncikar_MouseLeave);
            this.btncikar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btncikar_MouseMove);
            // 
            // num1
            // 
            this.num1.Location = new System.Drawing.Point(57, 23);
            this.num1.Multiline = true;
            this.num1.Name = "num1";
            this.num1.Size = new System.Drawing.Size(100, 20);
            this.num1.TabIndex = 7;
            this.num1.Text = "0";
            this.num1.TextChanged += new System.EventHandler(this.sayi_TextChanged);
            // 
            // num2
            // 
            this.num2.Location = new System.Drawing.Point(57, 54);
            this.num2.Multiline = true;
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(100, 20);
            this.num2.TabIndex = 8;
            this.num2.Text = "0";
            this.num2.TextChanged += new System.EventHandler(this.sayi2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Sonuç :";
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(62, 89);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(95, 21);
            this.result.TabIndex = 10;
            this.result.Text = "0";
            this.result.Click += new System.EventHandler(this.btnsonuc_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(285, 249);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.num2);
            this.Controls.Add(this.num1);
            this.Controls.Add(this.btncikar);
            this.Controls.Add(this.btntopla);
            this.Controls.Add(this.btnbol);
            this.Controls.Add(this.btnsonuc);
            this.Controls.Add(this.btncarp);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Name = "Form2";
            this.Text = "Hesap Makinesi";
           // this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Button btncarp;
        private System.Windows.Forms.Button btnsonuc;
        private System.Windows.Forms.Button btnbol;
        private System.Windows.Forms.Button btntopla;
        private System.Windows.Forms.Button btncikar;
        private System.Windows.Forms.TextBox num1;
        private System.Windows.Forms.TextBox num2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label result;
    }
}