﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    class CeaserCipher
    {
        Char[] Alphabet;



        public CeaserCipher()
        {
            Alphabet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        }

        public CeaserCipher(Char[] newAlphabet)
        {
            Alphabet = newAlphabet; // User can create new alphabet.
        }

        public string Enchiper(string input, int key)
        {
            if (input != "")   
            {

                char[] Message = input.ToCharArray();
                char[] encryptedMessage = new char[Message.Length];

                for (int i = 0; i < Message.Length; i++)
                {
                    char letter = Message[i];
                    int letterPosition = Array.IndexOf(Alphabet, letter);
                    if (letterPosition != -1)
                    {

                        int newLetterPosition = (letterPosition + key) % Alphabet.Length;
                        char letterEncoded = Alphabet[newLetterPosition];
                        encryptedMessage[i] = letterEncoded;
                    }
                    else
                    {
                        if (letter == ' ')
                        {
                            encryptedMessage[i] = letter;
                        }
                        else
                        {
                            return "Text does not match the alphabet...";
                        }
                    }
                   
                    
                }

                string encodedString = String.Join("", encryptedMessage);


                return encodedString;


            }
            else
                return "Please enter a message";

        }

        public string Dechiper(string inputEncoded, int key)
        {
            int x = Alphabet.Length - key;

            return Enchiper(inputEncoded, x);
        }

    }
}
