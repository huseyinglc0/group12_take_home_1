﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp3
{

    public partial class SignIn : Form
    {
        Form2 frm2 = new Form2(); // Create new form.
        AppChoosing ac = new AppChoosing();
        Form3 signUp = new Form3();

        // user dictionary used to store multiple users logs.

        Dictionary<string, string> users = new Dictionary<string, string>();
        
        private static List<User> listUser = new List<User>();
        User newUser = User.Instance();
        string path = @"user.csv";

        public SignIn()
        {
            InitializeComponent();
            File.LoadCsv(listUser, path);
         
        }       

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void leftFrameLbl_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private void label1_Click_2(object sender, EventArgs e)
        {
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void signInBtn_Click(object sender, EventArgs e)
        {     

            string userN = userNameTxt.Text;
            string passW = User.MD5Hash(passwordTxt.Text);

                try
                {
                    for (int i = 0; i < listUser.Count; i++)
                    {
                        newUser = listUser[i];
                        if (newUser.IsValid(userN, passW))
                        {                           
                                showMessageLbl.Text = "Successful";
                                showMessageLbl.ForeColor = Color.Green;

                                clearInputField();
                                timer1.Interval = 3000;
                                timer1.Enabled = true;
                                return;
                         }
                         else
                         {
                                showMessageLbl.Text = "Sorry, your password was incorrect. Please double-check your password.";
                                showMessageLbl.ForeColor = Color.Red;

                                clearInputField();
                         }                        
                    }
                    
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error", ex);
                }               

        }

        private void clearInputField()
        {
            userNameTxt.Text = "";
            passwordTxt.Text = "";
        }     

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ac.Visible = true;
            timer1.Enabled = false;
            this.Visible = false;
        }

        private void btn_SignUp_Click(object sender, EventArgs e)
        {
            signUp.Show();
            this.Visible = false;
        }
    }
    
}
