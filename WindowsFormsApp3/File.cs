﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WindowsFormsApp3
{
    class File
    {
        public static void LoadCsv(List<User> UserList, string path)
        {
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    var values = line.Split(',');
                    string username = values[0];
                    string password = values[1];
                    UserList.Add(new User(username, password));
                }
            }
        }

        public static void SaveCsv(List<User> UserList, string path)
        {
            using (var writter = new StreamWriter(path, true))
            {
                foreach (User user in UserList)
                {
                    writter.WriteLine(user.Tostring());
                    writter.Close();
                }
            }
        }
    }
}
