﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form4 : Form
    {
        
        GuessGame c = new GuessGame();

        public Form4()
        {
            InitializeComponent();
            
        }

        

        private void btn_reset_Click(object sender, EventArgs e)
        {
            c.reset();
            sonuc.Text = c.ToString();
        }

        private void btn_bati_Click(object sender, EventArgs e)
        {
            c.bati();
            sonuc.Text = c.ToString();
        }

        private void btn_dogu_Click(object sender, EventArgs e)
        {
            c.dogu();
            sonuc.Text = c.ToString();
        }

        private void btn_kuzey_Click(object sender, EventArgs e)
        {
            c.kuzey();
            sonuc.Text = c.ToString();
        }

        private void btn_guney_Click(object sender, EventArgs e)
        {
            c.guney();
            sonuc.Text = c.ToString();
        }

        private void btn_kdogu_Click(object sender, EventArgs e)
        {
            c.kuzeydogu();
            sonuc.Text = c.ToString();
        }

        private void btn_kbati_Click(object sender, EventArgs e)
        {
            c.kuzeybati();
            sonuc.Text = c.ToString();
        }

        private void btn_gbati_Click(object sender, EventArgs e)
        {
            c.guneybati();
            sonuc.Text = c.ToString();
        }

        private void btn_gdogu_Click(object sender, EventArgs e)
        {
            c.guneydogu();
            sonuc.Text = c.ToString();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            sonuc.Text = c.ToString();
        }
    }
}
