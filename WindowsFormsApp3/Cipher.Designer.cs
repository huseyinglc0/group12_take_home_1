﻿namespace WindowsFormsApp3
{
    partial class Cipher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBox_algrtm = new System.Windows.Forms.GroupBox();
            this.radioBtn_Vigenere = new System.Windows.Forms.RadioButton();
            this.radioBtn_Ceaser = new System.Windows.Forms.RadioButton();
            this.lbl_info = new System.Windows.Forms.Label();
            this.textBox_input = new System.Windows.Forms.TextBox();
            this.label_input = new System.Windows.Forms.Label();
            this.btn_encipher = new System.Windows.Forms.Button();
            this.label_resultCeaser = new System.Windows.Forms.Label();
            this.label_resultDecipher = new System.Windows.Forms.Label();
            this.btn_decipher = new System.Windows.Forms.Button();
            this.label_input2 = new System.Windows.Forms.Label();
            this.textBox_input2 = new System.Windows.Forms.TextBox();
            this.label_key = new System.Windows.Forms.Label();
            this.textBox_key = new System.Windows.Forms.TextBox();
            this.textBox_key2 = new System.Windows.Forms.TextBox();
            this.label_key2 = new System.Windows.Forms.Label();
            this.groupBox_ceaser = new System.Windows.Forms.GroupBox();
            this.groupBox_Decryption = new System.Windows.Forms.GroupBox();
            this.groupBox_Vigenere_encrypt = new System.Windows.Forms.GroupBox();
            this.groupBox_Vigenere_decrypt = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_deVig = new System.Windows.Forms.Button();
            this.textBox_inputVig2 = new System.Windows.Forms.TextBox();
            this.label_ResultdeVig = new System.Windows.Forms.Label();
            this.textBox_keyVig2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_inputVig = new System.Windows.Forms.TextBox();
            this.label_inputVi = new System.Windows.Forms.Label();
            this.btn_enVig = new System.Windows.Forms.Button();
            this.textBox_keyVig = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label_ResultVig = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_alphabe = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox_Cipher = new System.Windows.Forms.GroupBox();
            this.radioBtn_Decryption = new System.Windows.Forms.RadioButton();
            this.radioBtn_Encryption = new System.Windows.Forms.RadioButton();
            this.grpBox_algrtm.SuspendLayout();
            this.groupBox_ceaser.SuspendLayout();
            this.groupBox_Decryption.SuspendLayout();
            this.groupBox_Vigenere_encrypt.SuspendLayout();
            this.groupBox_Vigenere_decrypt.SuspendLayout();
            this.groupBox_Cipher.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBox_algrtm
            // 
            this.grpBox_algrtm.Controls.Add(this.radioBtn_Vigenere);
            this.grpBox_algrtm.Controls.Add(this.radioBtn_Ceaser);
            this.grpBox_algrtm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grpBox_algrtm.Location = new System.Drawing.Point(281, 12);
            this.grpBox_algrtm.Name = "grpBox_algrtm";
            this.grpBox_algrtm.Size = new System.Drawing.Size(333, 79);
            this.grpBox_algrtm.TabIndex = 0;
            this.grpBox_algrtm.TabStop = false;
            this.grpBox_algrtm.Text = "Algorithms";
            // 
            // radioBtn_Vigenere
            // 
            this.radioBtn_Vigenere.AutoSize = true;
            this.radioBtn_Vigenere.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.radioBtn_Vigenere.ForeColor = System.Drawing.Color.Black;
            this.radioBtn_Vigenere.Location = new System.Drawing.Point(156, 33);
            this.radioBtn_Vigenere.Name = "radioBtn_Vigenere";
            this.radioBtn_Vigenere.Size = new System.Drawing.Size(141, 21);
            this.radioBtn_Vigenere.TabIndex = 1;
            this.radioBtn_Vigenere.TabStop = true;
            this.radioBtn_Vigenere.Text = "Vigenère cipher";
            this.radioBtn_Vigenere.UseVisualStyleBackColor = false;
            this.radioBtn_Vigenere.Click += new System.EventHandler(this.radioBtn_Vigenere_Click);
            // 
            // radioBtn_Ceaser
            // 
            this.radioBtn_Ceaser.AutoCheck = false;
            this.radioBtn_Ceaser.AutoSize = true;
            this.radioBtn_Ceaser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.radioBtn_Ceaser.Checked = true;
            this.radioBtn_Ceaser.ForeColor = System.Drawing.Color.Black;
            this.radioBtn_Ceaser.Location = new System.Drawing.Point(7, 33);
            this.radioBtn_Ceaser.Name = "radioBtn_Ceaser";
            this.radioBtn_Ceaser.Size = new System.Drawing.Size(129, 21);
            this.radioBtn_Ceaser.TabIndex = 0;
            this.radioBtn_Ceaser.TabStop = true;
            this.radioBtn_Ceaser.Text = "Ceaser Cipher";
            this.radioBtn_Ceaser.UseVisualStyleBackColor = false;
            this.radioBtn_Ceaser.CheckedChanged += new System.EventHandler(this.radioBtn_Ceaser_CheckedChanged);
            this.radioBtn_Ceaser.Click += new System.EventHandler(this.radioBtn_Ceaser_Click);
            // 
            // lbl_info
            // 
            this.lbl_info.AutoSize = true;
            this.lbl_info.Font = new System.Drawing.Font("Microsoft JhengHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_info.Location = new System.Drawing.Point(1, 12);
            this.lbl_info.Name = "lbl_info";
            this.lbl_info.Size = new System.Drawing.Size(274, 18);
            this.lbl_info.TabIndex = 1;
            this.lbl_info.Text = "Please select the algorithm you will use.";
            // 
            // textBox_input
            // 
            this.textBox_input.Location = new System.Drawing.Point(72, 89);
            this.textBox_input.Name = "textBox_input";
            this.textBox_input.Size = new System.Drawing.Size(290, 23);
            this.textBox_input.TabIndex = 2;
            // 
            // label_input
            // 
            this.label_input.AutoSize = true;
            this.label_input.Location = new System.Drawing.Point(6, 89);
            this.label_input.Name = "label_input";
            this.label_input.Size = new System.Drawing.Size(39, 17);
            this.label_input.TabIndex = 3;
            this.label_input.Text = "Input";
            // 
            // btn_encipher
            // 
            this.btn_encipher.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_encipher.Location = new System.Drawing.Point(9, 161);
            this.btn_encipher.Name = "btn_encipher";
            this.btn_encipher.Size = new System.Drawing.Size(75, 35);
            this.btn_encipher.TabIndex = 4;
            this.btn_encipher.Text = "Encrypt";
            this.btn_encipher.UseVisualStyleBackColor = true;
            this.btn_encipher.Click += new System.EventHandler(this.btn_encipher_Click);
            // 
            // label_resultCeaser
            // 
            this.label_resultCeaser.AutoSize = true;
            this.label_resultCeaser.Location = new System.Drawing.Point(157, 167);
            this.label_resultCeaser.MaximumSize = new System.Drawing.Size(20, 20);
            this.label_resultCeaser.MinimumSize = new System.Drawing.Size(600, 80);
            this.label_resultCeaser.Name = "label_resultCeaser";
            this.label_resultCeaser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_resultCeaser.Size = new System.Drawing.Size(600, 80);
            this.label_resultCeaser.TabIndex = 5;
            // 
            // label_resultDecipher
            // 
            this.label_resultDecipher.AutoSize = true;
            this.label_resultDecipher.Location = new System.Drawing.Point(135, 135);
            this.label_resultDecipher.Name = "label_resultDecipher";
            this.label_resultDecipher.Size = new System.Drawing.Size(0, 17);
            this.label_resultDecipher.TabIndex = 1;
            // 
            // btn_decipher
            // 
            this.btn_decipher.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_decipher.Location = new System.Drawing.Point(9, 123);
            this.btn_decipher.Name = "btn_decipher";
            this.btn_decipher.Size = new System.Drawing.Size(75, 35);
            this.btn_decipher.TabIndex = 8;
            this.btn_decipher.Text = "Decrypt";
            this.btn_decipher.UseVisualStyleBackColor = true;
            this.btn_decipher.Click += new System.EventHandler(this.btn_decipher_Click);
            // 
            // label_input2
            // 
            this.label_input2.AutoSize = true;
            this.label_input2.Location = new System.Drawing.Point(6, 50);
            this.label_input2.Name = "label_input2";
            this.label_input2.Size = new System.Drawing.Size(39, 17);
            this.label_input2.TabIndex = 7;
            this.label_input2.Text = "Input";
            // 
            // textBox_input2
            // 
            this.textBox_input2.Location = new System.Drawing.Point(76, 47);
            this.textBox_input2.Name = "textBox_input2";
            this.textBox_input2.Size = new System.Drawing.Size(275, 23);
            this.textBox_input2.TabIndex = 6;
            // 
            // label_key
            // 
            this.label_key.AutoSize = true;
            this.label_key.Location = new System.Drawing.Point(6, 121);
            this.label_key.Name = "label_key";
            this.label_key.Size = new System.Drawing.Size(68, 17);
            this.label_key.TabIndex = 10;
            this.label_key.Text = "Rot value";
            // 
            // textBox_key
            // 
            this.textBox_key.Location = new System.Drawing.Point(72, 121);
            this.textBox_key.Name = "textBox_key";
            this.textBox_key.Size = new System.Drawing.Size(100, 23);
            this.textBox_key.TabIndex = 11;
            this.textBox_key.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_key_KeyPress);
            // 
            // textBox_key2
            // 
            this.textBox_key2.Location = new System.Drawing.Point(76, 84);
            this.textBox_key2.Name = "textBox_key2";
            this.textBox_key2.Size = new System.Drawing.Size(100, 23);
            this.textBox_key2.TabIndex = 13;
            this.textBox_key2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_key2_KeyPress);
            // 
            // label_key2
            // 
            this.label_key2.AutoSize = true;
            this.label_key2.Location = new System.Drawing.Point(6, 87);
            this.label_key2.Name = "label_key2";
            this.label_key2.Size = new System.Drawing.Size(68, 17);
            this.label_key2.TabIndex = 12;
            this.label_key2.Text = "Rot value";
            // 
            // groupBox_ceaser
            // 
            this.groupBox_ceaser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.groupBox_ceaser.Controls.Add(this.textBox_alphabe);
            this.groupBox_ceaser.Controls.Add(this.label8);
            this.groupBox_ceaser.Controls.Add(this.label1);
            this.groupBox_ceaser.Controls.Add(this.textBox_input);
            this.groupBox_ceaser.Controls.Add(this.label_input);
            this.groupBox_ceaser.Controls.Add(this.btn_encipher);
            this.groupBox_ceaser.Controls.Add(this.textBox_key);
            this.groupBox_ceaser.Controls.Add(this.label_resultCeaser);
            this.groupBox_ceaser.Controls.Add(this.label_key);
            this.groupBox_ceaser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox_ceaser.Location = new System.Drawing.Point(12, 160);
            this.groupBox_ceaser.Name = "groupBox_ceaser";
            this.groupBox_ceaser.Size = new System.Drawing.Size(817, 216);
            this.groupBox_ceaser.TabIndex = 14;
            this.groupBox_ceaser.TabStop = false;
            this.groupBox_ceaser.Text = "Ceaser Cipher Encryption";
            this.groupBox_ceaser.Enter += new System.EventHandler(this.groupBox_ceaser_Enter);
            // 
            // groupBox_Decryption
            // 
            this.groupBox_Decryption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.groupBox_Decryption.Controls.Add(this.label_input2);
            this.groupBox_Decryption.Controls.Add(this.btn_decipher);
            this.groupBox_Decryption.Controls.Add(this.label3);
            this.groupBox_Decryption.Controls.Add(this.label_resultDecipher);
            this.groupBox_Decryption.Controls.Add(this.textBox_input2);
            this.groupBox_Decryption.Controls.Add(this.label_key2);
            this.groupBox_Decryption.Controls.Add(this.textBox_key2);
            this.groupBox_Decryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox_Decryption.Location = new System.Drawing.Point(25, 171);
            this.groupBox_Decryption.Name = "groupBox_Decryption";
            this.groupBox_Decryption.Size = new System.Drawing.Size(801, 182);
            this.groupBox_Decryption.TabIndex = 17;
            this.groupBox_Decryption.TabStop = false;
            this.groupBox_Decryption.Text = "Ceaser Cipher Decryption";
            // 
            // groupBox_Vigenere_encrypt
            // 
            this.groupBox_Vigenere_encrypt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.groupBox_Vigenere_encrypt.Controls.Add(this.label5);
            this.groupBox_Vigenere_encrypt.Controls.Add(this.textBox_inputVig);
            this.groupBox_Vigenere_encrypt.Controls.Add(this.label_inputVi);
            this.groupBox_Vigenere_encrypt.Controls.Add(this.btn_enVig);
            this.groupBox_Vigenere_encrypt.Controls.Add(this.textBox_keyVig);
            this.groupBox_Vigenere_encrypt.Controls.Add(this.label4);
            this.groupBox_Vigenere_encrypt.Controls.Add(this.label_ResultVig);
            this.groupBox_Vigenere_encrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox_Vigenere_encrypt.Location = new System.Drawing.Point(14, 182);
            this.groupBox_Vigenere_encrypt.Name = "groupBox_Vigenere_encrypt";
            this.groupBox_Vigenere_encrypt.Size = new System.Drawing.Size(806, 101);
            this.groupBox_Vigenere_encrypt.TabIndex = 15;
            this.groupBox_Vigenere_encrypt.TabStop = false;
            this.groupBox_Vigenere_encrypt.Text = "Vigenère Cipher Encryption ";
            // 
            // groupBox_Vigenere_decrypt
            // 
            this.groupBox_Vigenere_decrypt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.groupBox_Vigenere_decrypt.Controls.Add(this.label7);
            this.groupBox_Vigenere_decrypt.Controls.Add(this.btn_deVig);
            this.groupBox_Vigenere_decrypt.Controls.Add(this.textBox_inputVig2);
            this.groupBox_Vigenere_decrypt.Controls.Add(this.label_ResultdeVig);
            this.groupBox_Vigenere_decrypt.Controls.Add(this.textBox_keyVig2);
            this.groupBox_Vigenere_decrypt.Controls.Add(this.label2);
            this.groupBox_Vigenere_decrypt.Controls.Add(this.label6);
            this.groupBox_Vigenere_decrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox_Vigenere_decrypt.Location = new System.Drawing.Point(17, 199);
            this.groupBox_Vigenere_decrypt.Name = "groupBox_Vigenere_decrypt";
            this.groupBox_Vigenere_decrypt.Size = new System.Drawing.Size(808, 101);
            this.groupBox_Vigenere_decrypt.TabIndex = 18;
            this.groupBox_Vigenere_decrypt.TabStop = false;
            this.groupBox_Vigenere_decrypt.Text = "Vigenère Cipher Decryption";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(449, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 17);
            this.label7.TabIndex = 27;
            this.label7.Text = ">>";
            // 
            // btn_deVig
            // 
            this.btn_deVig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_deVig.Location = new System.Drawing.Point(365, 30);
            this.btn_deVig.Name = "btn_deVig";
            this.btn_deVig.Size = new System.Drawing.Size(75, 35);
            this.btn_deVig.TabIndex = 20;
            this.btn_deVig.Text = "Decrypt";
            this.btn_deVig.UseVisualStyleBackColor = true;
            this.btn_deVig.Click += new System.EventHandler(this.btn_deVig_Click);
            // 
            // textBox_inputVig2
            // 
            this.textBox_inputVig2.Location = new System.Drawing.Point(54, 36);
            this.textBox_inputVig2.Name = "textBox_inputVig2";
            this.textBox_inputVig2.Size = new System.Drawing.Size(290, 23);
            this.textBox_inputVig2.TabIndex = 18;
            this.textBox_inputVig2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_inputVig2_KeyPress);
            // 
            // label_ResultdeVig
            // 
            this.label_ResultdeVig.AutoSize = true;
            this.label_ResultdeVig.Location = new System.Drawing.Point(485, 36);
            this.label_ResultdeVig.Name = "label_ResultdeVig";
            this.label_ResultdeVig.Size = new System.Drawing.Size(0, 17);
            this.label_ResultdeVig.TabIndex = 21;
            // 
            // textBox_keyVig2
            // 
            this.textBox_keyVig2.Location = new System.Drawing.Point(54, 68);
            this.textBox_keyVig2.Name = "textBox_keyVig2";
            this.textBox_keyVig2.Size = new System.Drawing.Size(100, 23);
            this.textBox_keyVig2.TabIndex = 25;
            this.textBox_keyVig2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_keyVig2_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "Key";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "İnput";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(440, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 17);
            this.label5.TabIndex = 26;
            this.label5.Text = ">>";
            // 
            // textBox_inputVig
            // 
            this.textBox_inputVig.Location = new System.Drawing.Point(47, 19);
            this.textBox_inputVig.Name = "textBox_inputVig";
            this.textBox_inputVig.Size = new System.Drawing.Size(290, 23);
            this.textBox_inputVig.TabIndex = 14;
            this.textBox_inputVig.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_inputVig_KeyPress);
            // 
            // label_inputVi
            // 
            this.label_inputVi.AutoSize = true;
            this.label_inputVi.Location = new System.Drawing.Point(10, 25);
            this.label_inputVi.Name = "label_inputVi";
            this.label_inputVi.Size = new System.Drawing.Size(39, 17);
            this.label_inputVi.TabIndex = 15;
            this.label_inputVi.Text = "İnput";
            // 
            // btn_enVig
            // 
            this.btn_enVig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_enVig.Location = new System.Drawing.Point(358, 11);
            this.btn_enVig.Name = "btn_enVig";
            this.btn_enVig.Size = new System.Drawing.Size(75, 35);
            this.btn_enVig.TabIndex = 16;
            this.btn_enVig.Text = "Encrypt";
            this.btn_enVig.UseVisualStyleBackColor = true;
            this.btn_enVig.Click += new System.EventHandler(this.btn_enVig_Click);
            // 
            // textBox_keyVig
            // 
            this.textBox_keyVig.Location = new System.Drawing.Point(47, 52);
            this.textBox_keyVig.Name = "textBox_keyVig";
            this.textBox_keyVig.Size = new System.Drawing.Size(100, 23);
            this.textBox_keyVig.TabIndex = 23;
            this.textBox_keyVig.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_keyVig_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Key";
            // 
            // label_ResultVig
            // 
            this.label_ResultVig.AutoSize = true;
            this.label_ResultVig.Location = new System.Drawing.Point(470, 25);
            this.label_ResultVig.Name = "label_ResultVig";
            this.label_ResultVig.Size = new System.Drawing.Size(0, 17);
            this.label_ResultVig.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = ">>";
            // 
            // textBox_alphabe
            // 
            this.textBox_alphabe.Location = new System.Drawing.Point(72, 50);
            this.textBox_alphabe.Multiline = true;
            this.textBox_alphabe.Name = "textBox_alphabe";
            this.textBox_alphabe.Size = new System.Drawing.Size(290, 23);
            this.textBox_alphabe.TabIndex = 17;
            this.textBox_alphabe.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "Alphabe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(105, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = ">>";
            // 
            // groupBox_Cipher
            // 
            this.groupBox_Cipher.Controls.Add(this.radioBtn_Decryption);
            this.groupBox_Cipher.Controls.Add(this.radioBtn_Encryption);
            this.groupBox_Cipher.Location = new System.Drawing.Point(281, 97);
            this.groupBox_Cipher.Name = "groupBox_Cipher";
            this.groupBox_Cipher.Size = new System.Drawing.Size(333, 57);
            this.groupBox_Cipher.TabIndex = 16;
            this.groupBox_Cipher.TabStop = false;
            // 
            // radioBtn_Decryption
            // 
            this.radioBtn_Decryption.AutoSize = true;
            this.radioBtn_Decryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioBtn_Decryption.Location = new System.Drawing.Point(156, 19);
            this.radioBtn_Decryption.MinimumSize = new System.Drawing.Size(129, 21);
            this.radioBtn_Decryption.Name = "radioBtn_Decryption";
            this.radioBtn_Decryption.Size = new System.Drawing.Size(129, 21);
            this.radioBtn_Decryption.TabIndex = 1;
            this.radioBtn_Decryption.TabStop = true;
            this.radioBtn_Decryption.Text = "Decryption";
            this.radioBtn_Decryption.UseVisualStyleBackColor = true;
            this.radioBtn_Decryption.CheckedChanged += new System.EventHandler(this.radioBtn_Decryption_CheckedChanged);
            // 
            // radioBtn_Encryption
            // 
            this.radioBtn_Encryption.AutoSize = true;
            this.radioBtn_Encryption.Checked = true;
            this.radioBtn_Encryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioBtn_Encryption.Location = new System.Drawing.Point(7, 19);
            this.radioBtn_Encryption.MinimumSize = new System.Drawing.Size(129, 21);
            this.radioBtn_Encryption.Name = "radioBtn_Encryption";
            this.radioBtn_Encryption.Size = new System.Drawing.Size(129, 21);
            this.radioBtn_Encryption.TabIndex = 0;
            this.radioBtn_Encryption.TabStop = true;
            this.radioBtn_Encryption.Text = "Encryption";
            this.radioBtn_Encryption.UseVisualStyleBackColor = true;
            this.radioBtn_Encryption.CheckedChanged += new System.EventHandler(this.radioBtn_Encryption_CheckedChanged);
            // 
            // Cipher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(832, 390);
            this.Controls.Add(this.groupBox_Decryption);
            this.Controls.Add(this.groupBox_Vigenere_decrypt);
            this.Controls.Add(this.groupBox_Vigenere_encrypt);
            this.Controls.Add(this.groupBox_Cipher);
            this.Controls.Add(this.groupBox_ceaser);
            this.Controls.Add(this.lbl_info);
            this.Controls.Add(this.grpBox_algrtm);
            this.Name = "Cipher";
            this.Text = "Cipher";
            this.Load += new System.EventHandler(this.Cipher_Load);
            this.grpBox_algrtm.ResumeLayout(false);
            this.grpBox_algrtm.PerformLayout();
            this.groupBox_ceaser.ResumeLayout(false);
            this.groupBox_ceaser.PerformLayout();
            this.groupBox_Decryption.ResumeLayout(false);
            this.groupBox_Decryption.PerformLayout();
            this.groupBox_Vigenere_encrypt.ResumeLayout(false);
            this.groupBox_Vigenere_encrypt.PerformLayout();
            this.groupBox_Vigenere_decrypt.ResumeLayout(false);
            this.groupBox_Vigenere_decrypt.PerformLayout();
            this.groupBox_Cipher.ResumeLayout(false);
            this.groupBox_Cipher.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBox_algrtm;
        private System.Windows.Forms.RadioButton radioBtn_Vigenere;
        private System.Windows.Forms.RadioButton radioBtn_Ceaser;
        private System.Windows.Forms.Label lbl_info;
        private System.Windows.Forms.TextBox textBox_input;
        private System.Windows.Forms.Label label_input;
        private System.Windows.Forms.Button btn_encipher;
        private System.Windows.Forms.Label label_resultDecipher;
        private System.Windows.Forms.Button btn_decipher;
        private System.Windows.Forms.Label label_input2;
        private System.Windows.Forms.TextBox textBox_input2;
        private System.Windows.Forms.Label label_key;
        private System.Windows.Forms.TextBox textBox_key;
        private System.Windows.Forms.TextBox textBox_key2;
        private System.Windows.Forms.Label label_key2;
        private System.Windows.Forms.GroupBox groupBox_ceaser;
        private System.Windows.Forms.GroupBox groupBox_Vigenere_encrypt;
        private System.Windows.Forms.TextBox textBox_inputVig;
        private System.Windows.Forms.TextBox textBox_keyVig2;
        private System.Windows.Forms.Button btn_deVig;
        private System.Windows.Forms.Label label_inputVi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_ResultdeVig;
        private System.Windows.Forms.Button btn_enVig;
        private System.Windows.Forms.TextBox textBox_inputVig2;
        private System.Windows.Forms.TextBox textBox_keyVig;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_ResultVig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label_resultCeaser;
        private System.Windows.Forms.GroupBox groupBox_Cipher;
        private System.Windows.Forms.RadioButton radioBtn_Decryption;
        private System.Windows.Forms.RadioButton radioBtn_Encryption;
        private System.Windows.Forms.GroupBox groupBox_Decryption;
        private System.Windows.Forms.TextBox textBox_alphabe;
        private System.Windows.Forms.GroupBox groupBox_Vigenere_decrypt;
    }
}