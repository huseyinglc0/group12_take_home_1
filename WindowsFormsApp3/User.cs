﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace WindowsFormsApp3
{
    class User
    {
        public string Username;
        public string Password;
        public string Confirmpassword;

        public User()
        {
        }

        public User(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public User(string username, string password, string confirmpassword)
        {
            Username = username;
            Password = password;
            Confirmpassword = confirmpassword;
        }

        private static User instance = null;
        public static User Instance()
        {
            if (instance == null)
            {
                instance = new User();
            }
            return instance;
        }

        public string Tostring()
        {
            return Username + "," + Password + "," + Confirmpassword ;            
        }

        public static string MD5Hash(string pass)
        {           
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();            
            byte[] dizi = Encoding.UTF8.GetBytes(pass);            
            dizi = md5.ComputeHash(dizi);            
            StringBuilder sb = new StringBuilder();            

            foreach (byte ba in dizi)
            {
                sb.Append(ba.ToString("x2").ToLower());
            }
           
            return sb.ToString();
        }

        public bool IsValid(string username, string password)
        {
            return this.Username.Equals(username) && this.Password.Equals(password);
        }

    }




}

