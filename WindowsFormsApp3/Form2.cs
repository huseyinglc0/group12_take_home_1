﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {   

        Calculator Cal = Calculator.Instance();
        double sayi1 = 0;
        double sayi2 = 0;
        double sonuc = 0;
        bool error = false; //Using for divide by zero error.
        public Form2()
        {
            InitializeComponent();
        }


        private void btncikar_Click(object sender, EventArgs e)
        {
            sayi1 = Convert.ToDouble(num1.Text);
            sayi2 = Convert.ToDouble(num2.Text);
            sonuc = Cal.Cıkarma(sayi1, sayi2);
            


            btnbol.BackColor = Color.White;    
            btncarp.BackColor = Color.White;
            btncikar.BackColor = Color.Gold;       // Last used button colors change.
            btntopla.BackColor = Color.White;
            btnsonuc.BackColor = Color.White;   
        }
        
        private void btntopla_Click(object sender, EventArgs e)
        {
            
            
            sayi1 = Convert.ToDouble(num1.Text);
            sayi2 = Convert.ToDouble(num2.Text);
            sonuc = Cal.Toplama(sayi1, sayi2);
            
           


            btnbol.BackColor = Color.White;
            btncarp.BackColor = Color.White;
            btncikar.BackColor = Color.White;        
            btntopla.BackColor = Color.Gold;
            btnsonuc.BackColor = Color.White;



        }

        private void sayi_TextChanged(object sender, EventArgs e)
        {

        }
       
        private void sayi2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnsonuc_Click(object sender, EventArgs e)
        {
            result.Text = Convert.ToString(sonuc);
            if (sonuc < 0)
            {
                result.BackColor = Color.Red;
            }
            else result.BackColor = Color.Green;
            if (error == true)
            {
                result.Text = "Sıfıra Bölünemez";
                result.BackColor = Color.Yellow;
                error = false;
            }

            btnbol.BackColor = Color.White;
            btncarp.BackColor = Color.White;
            btncikar.BackColor = Color.White;
            btntopla.BackColor = Color.White;
            btnsonuc.BackColor = Color.Gold;



        }

        private void btnbol_Click(object sender, EventArgs e)
        {
            sayi1 = Convert.ToDouble(num1.Text);
            sayi2 = Convert.ToDouble(num2.Text);
            if (sayi1 == 0||sayi2==0)
            {
                error = true;
            }
            else
            sonuc = Cal.Bolme(sayi1, sayi2);


            btnbol.BackColor = Color.Gold;
            btncarp.BackColor = Color.White;
            btncikar.BackColor = Color.White;
            btntopla.BackColor = Color.White;
            btnsonuc.BackColor = Color.White;

        }

        private void btncarp_Click(object sender, EventArgs e)
        {
            sayi1 = Convert.ToDouble(num1.Text);
            sayi2 = Convert.ToDouble(num2.Text);
            sonuc = Cal.Carpma(sayi1, sayi2);


            btnbol.BackColor = Color.White;
            btncarp.BackColor = Color.Gold;
            btncikar.BackColor = Color.White;
            btntopla.BackColor = Color.White;
            btnsonuc.BackColor = Color.White;

        }

        private void result_Click(object sender, EventArgs e)
        {
           
        }

        private void btnsonuc_MouseMove(object sender, MouseEventArgs e)
        {
            this.btnsonuc.ForeColor = Color.Red;        //When mouse is over this button , change text color.
        }

        private void btnsonuc_MouseLeave(object sender, EventArgs e)
        {
            this.btnsonuc.ForeColor = Color.Black;     //When mouse is leave this button , change text color.
        }

        private void btnbol_MouseMove(object sender, MouseEventArgs e)
        {
            this.btnbol.ForeColor = Color.Red;
        }

        private void btnbol_MouseLeave(object sender, EventArgs e)
        {
            this.btnbol.ForeColor = Color.Black;
        }

        private void btncarp_MouseMove(object sender, MouseEventArgs e)
        {
            this.btncarp.ForeColor = Color.Red;
        }

        private void btncarp_MouseLeave(object sender, EventArgs e)
        {
            this.btncarp.ForeColor = Color.Black;
        }

        private void btncikar_MouseMove(object sender, MouseEventArgs e)
        {
            this.btncikar.ForeColor = Color.Red;
        }

        private void btncikar_MouseLeave(object sender, EventArgs e)
        {
            this.btncikar.ForeColor = Color.Black;
        }

        private void btntopla_MouseMove(object sender, MouseEventArgs e)
        {
            this.btntopla.ForeColor = Color.Red;
        }

        private void btntopla_MouseLeave(object sender, EventArgs e)
        {
            this.btntopla.ForeColor = Color.Black;
        }
    }
    class Calculator
    {
        static private Calculator instance;
     
        public Calculator()
        {
            
        }

        public double Toplama(double sayi1, double sayi2)
        {
            return sayi1 + sayi2;
        }
        public double Bolme(double sayi1, double sayi2)
        {
            return sayi1 / sayi2;
        }
        public double Carpma(double sayi1, double sayi2)
        {
            return sayi1 * sayi2;
        }
        public double Cıkarma(double sayi1, double sayi2)
        {
            return sayi1 - sayi2;
        }
        public static Calculator Instance()
        {
            if (instance == null)
            {
                instance = new Calculator();
            }
            return instance;
        }
    }

}