﻿
namespace WindowsFormsApp3
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_start = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.sonuc = new System.Windows.Forms.Label();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_gdogu = new System.Windows.Forms.Button();
            this.btn_gbati = new System.Windows.Forms.Button();
            this.btn_kdogu = new System.Windows.Forms.Button();
            this.btn_kbati = new System.Windows.Forms.Button();
            this.btn_guney = new System.Windows.Forms.Button();
            this.btn_kuzey = new System.Windows.Forms.Button();
            this.btn_dogu = new System.Windows.Forms.Button();
            this.btn_bati = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_start.Location = new System.Drawing.Point(24, 23);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(69, 41);
            this.btn_start.TabIndex = 23;
            this.btn_start.Text = "START";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(367, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 28);
            this.button1.TabIndex = 22;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // sonuc
            // 
            this.sonuc.AutoSize = true;
            this.sonuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sonuc.Location = new System.Drawing.Point(116, 89);
            this.sonuc.MinimumSize = new System.Drawing.Size(50, 50);
            this.sonuc.Name = "sonuc";
            this.sonuc.Size = new System.Drawing.Size(223, 50);
            this.sonuc.TabIndex = 21;
            this.sonuc.Text = "Press start button.";
            // 
            // btn_reset
            // 
            this.btn_reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_reset.Location = new System.Drawing.Point(157, 221);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(102, 51);
            this.btn_reset.TabIndex = 20;
            this.btn_reset.Text = "RESET";
            this.btn_reset.UseVisualStyleBackColor = false;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btn_gdogu
            // 
            this.btn_gdogu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_gdogu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_gdogu.Location = new System.Drawing.Point(265, 278);
            this.btn_gdogu.Name = "btn_gdogu";
            this.btn_gdogu.Size = new System.Drawing.Size(102, 51);
            this.btn_gdogu.TabIndex = 19;
            this.btn_gdogu.Text = "GÜNEYDOĞU";
            this.btn_gdogu.UseVisualStyleBackColor = false;
            this.btn_gdogu.Click += new System.EventHandler(this.btn_gdogu_Click);
            // 
            // btn_gbati
            // 
            this.btn_gbati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_gbati.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_gbati.Location = new System.Drawing.Point(49, 278);
            this.btn_gbati.Name = "btn_gbati";
            this.btn_gbati.Size = new System.Drawing.Size(102, 51);
            this.btn_gbati.TabIndex = 18;
            this.btn_gbati.Text = "GÜNEYBATI";
            this.btn_gbati.UseVisualStyleBackColor = false;
            this.btn_gbati.Click += new System.EventHandler(this.btn_gbati_Click);
            // 
            // btn_kdogu
            // 
            this.btn_kdogu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_kdogu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_kdogu.Location = new System.Drawing.Point(265, 164);
            this.btn_kdogu.Name = "btn_kdogu";
            this.btn_kdogu.Size = new System.Drawing.Size(102, 51);
            this.btn_kdogu.TabIndex = 17;
            this.btn_kdogu.Text = "KUZEYDOĞU";
            this.btn_kdogu.UseVisualStyleBackColor = false;
            this.btn_kdogu.Click += new System.EventHandler(this.btn_kdogu_Click);
            // 
            // btn_kbati
            // 
            this.btn_kbati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_kbati.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_kbati.Location = new System.Drawing.Point(49, 163);
            this.btn_kbati.Name = "btn_kbati";
            this.btn_kbati.Size = new System.Drawing.Size(102, 52);
            this.btn_kbati.TabIndex = 16;
            this.btn_kbati.Text = "KUZEYBATI";
            this.btn_kbati.UseVisualStyleBackColor = false;
            this.btn_kbati.Click += new System.EventHandler(this.btn_kbati_Click);
            // 
            // btn_guney
            // 
            this.btn_guney.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_guney.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_guney.Location = new System.Drawing.Point(157, 278);
            this.btn_guney.Name = "btn_guney";
            this.btn_guney.Size = new System.Drawing.Size(102, 51);
            this.btn_guney.TabIndex = 15;
            this.btn_guney.Text = "GÜNEY";
            this.btn_guney.UseVisualStyleBackColor = false;
            this.btn_guney.Click += new System.EventHandler(this.btn_guney_Click);
            // 
            // btn_kuzey
            // 
            this.btn_kuzey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_kuzey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_kuzey.Location = new System.Drawing.Point(157, 164);
            this.btn_kuzey.Name = "btn_kuzey";
            this.btn_kuzey.Size = new System.Drawing.Size(102, 51);
            this.btn_kuzey.TabIndex = 14;
            this.btn_kuzey.Text = "KUZEY";
            this.btn_kuzey.UseVisualStyleBackColor = false;
            this.btn_kuzey.Click += new System.EventHandler(this.btn_kuzey_Click);
            // 
            // btn_dogu
            // 
            this.btn_dogu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_dogu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_dogu.Location = new System.Drawing.Point(265, 221);
            this.btn_dogu.Name = "btn_dogu";
            this.btn_dogu.Size = new System.Drawing.Size(102, 51);
            this.btn_dogu.TabIndex = 13;
            this.btn_dogu.Text = "DOĞU";
            this.btn_dogu.UseVisualStyleBackColor = false;
            this.btn_dogu.Click += new System.EventHandler(this.btn_dogu_Click);
            // 
            // btn_bati
            // 
            this.btn_bati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_bati.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_bati.Location = new System.Drawing.Point(49, 221);
            this.btn_bati.Name = "btn_bati";
            this.btn_bati.Size = new System.Drawing.Size(102, 51);
            this.btn_bati.TabIndex = 12;
            this.btn_bati.Text = "BATI";
            this.btn_bati.UseVisualStyleBackColor = false;
            this.btn_bati.Click += new System.EventHandler(this.btn_bati_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 381);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.sonuc);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.btn_gdogu);
            this.Controls.Add(this.btn_gbati);
            this.Controls.Add(this.btn_kdogu);
            this.Controls.Add(this.btn_kbati);
            this.Controls.Add(this.btn_guney);
            this.Controls.Add(this.btn_kuzey);
            this.Controls.Add(this.btn_dogu);
            this.Controls.Add(this.btn_bati);
            this.Name = "Form4";
            this.Text = "Tahmin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label sonuc;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_gdogu;
        private System.Windows.Forms.Button btn_gbati;
        private System.Windows.Forms.Button btn_kdogu;
        private System.Windows.Forms.Button btn_kbati;
        private System.Windows.Forms.Button btn_guney;
        private System.Windows.Forms.Button btn_kuzey;
        private System.Windows.Forms.Button btn_dogu;
        private System.Windows.Forms.Button btn_bati;
    }
}