﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Cipher : Form
    {
        CeaserCipher cipher = new CeaserCipher();

        
        public Cipher()
        {

            InitializeComponent();
            groupBox_ceaser.Visible = false;
            groupBox_Vigenere_encrypt.Visible = false;
            groupBox_Decryption.Visible = false;
            groupBox_Vigenere_decrypt.Visible = false;
        }

        private void Cipher_Load(object sender, EventArgs e)
        {
            groupBox_ceaser.Visible = false;
            groupBox_Vigenere_encrypt.Visible = false;
            groupBox_Decryption.Visible = false;
            groupBox_Cipher.Visible = true;
            grpBox_algrtm.Visible = true;
        }


        private void btn_encipher_Click(object sender, EventArgs e)
        {
            if (textBox_input.Text == "" || textBox_key.Text == "")
            {
                MessageBox.Show("Input and Rot value can not be empty!!!", "Empty Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                string result;
                string input;
                int key;

                if (textBox_alphabe.Text != "")
                {

                    input = textBox_input.Text;
                    string alphabe = textBox_alphabe.Text.ToLower();
                    key = Convert.ToInt32(textBox_key.Text);
                    char[] newAlphabe = alphabe.ToCharArray();                    

                    CeaserCipher cipher2 = new CeaserCipher(newAlphabe);
                    result = cipher2.Enchiper(input, key);
                    label_resultCeaser.Text = result;
                    
                }

                else
                {
                    input = textBox_input.Text;
                    key = Convert.ToInt32(textBox_key.Text);
                    result = cipher.Enchiper(input, key);
                    label_resultCeaser.Text = result;
                }
            }
        }

        private void btn_decipher_Click(object sender, EventArgs e)
        {
            string alphabe = textBox_alphabe.Text.ToLower();
            char[] newAlphabe = alphabe.ToCharArray();
            CeaserCipher cipher2 = new CeaserCipher(newAlphabe);
            string input2;
            input2 = textBox_input2.Text;
            int key;
            key = Convert.ToInt32(textBox_key2.Text);
            string result2 = cipher2.Dechiper(input2, key);
            label_resultDecipher.Text = result2;
        }

        private void btn_enVig_Click(object sender, EventArgs e)
        {
            string input = textBox_inputVig.Text.ToLower();
            string key = textBox_keyVig.Text.ToLower();
            
            string result= VigenereCipher.Encipher(input, key);
            label_ResultVig.Text = result;
        }

        private void btn_deVig_Click(object sender, EventArgs e)
        {
            string input2 = textBox_inputVig2.Text.ToLower();
            string key2 = textBox_keyVig2.Text.ToLower();
            string result2 = VigenereCipher.Decipher(input2, key2);
            label_ResultdeVig.Text = result2;
        }


        private void groupBox_ceaser_Enter(object sender, EventArgs e)
        {

        }

        private void radioBtn_Ceaser_CheckedChanged(object sender, EventArgs e)
        {
            groupBox_ceaser.Visible = true;
            radioBtn_Encryption.Checked = true;
        }


        private void radioBtn_Encryption_CheckedChanged(object sender, EventArgs e)
        {
            if (radioBtn_Vigenere.Checked == true)
            {
                groupBox_Vigenere_encrypt.Visible = true;
                groupBox_Decryption.Visible = false;
                groupBox_ceaser.Visible = false;
                groupBox_Vigenere_decrypt.Visible = false;

            }
            else
            {
                groupBox_Decryption.Visible = false;
                groupBox_ceaser.Visible = true;
            }
            
        }

        private void radioBtn_Decryption_CheckedChanged(object sender, EventArgs e)
        {
            if (radioBtn_Vigenere.Checked == true)
            {
                groupBox_Vigenere_decrypt.Visible = true;
                groupBox_Decryption.Visible = false;
                groupBox_ceaser.Visible = false;
                groupBox_Vigenere_encrypt.Visible = false;

            }
            //if (radioBtn_Decryption.Checked == true) 
            //{
            //    groupBox_Decryption.Visible = true;
            //    groupBox_ceaser.Visible = false;
            //}
            else
            {
                groupBox_Decryption.Visible = true;
                groupBox_ceaser.Visible = false;
            }

        }

        private void radioBtn_Ceaser_Click(object sender, EventArgs e)
        {
            radioBtn_Ceaser.Checked = true;
            radioBtn_Vigenere.Checked = false;
            groupBox_ceaser.Visible = true;
            groupBox_Vigenere_encrypt.Visible = false;
        }

        private void radioBtn_Vigenere_Click(object sender, EventArgs e)
        {
            
            radioBtn_Vigenere.Checked = true;
            radioBtn_Ceaser.Checked = false;
            groupBox_Vigenere_encrypt.Visible = true;
            groupBox_ceaser.Visible = false;
        }

        private void textBox_key2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox_key_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox_inputVig2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsSeparator(e.KeyChar);
        }

        private void textBox_keyVig2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsSeparator(e.KeyChar);
        }

        private void textBox_inputVig_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsSeparator(e.KeyChar);
        }

        private void textBox_keyVig_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsSeparator(e.KeyChar);
        }
    }
       
}
